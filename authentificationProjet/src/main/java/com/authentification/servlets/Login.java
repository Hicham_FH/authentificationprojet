package com.authentification.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.authentification.models.Role;
import com.authentification.models.Utilisateur;

/**
 * Servlet implementation class Login
 */
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
			//========  Liste Des Utilistateur ==================//
	
	static List<Utilisateur> listUsers = new ArrayList<Utilisateur>();
	
	 static {
		 
		 Utilisateur u1 = new Utilisateur("talbi", "fethi", "fethi1", "0000",Role.Admin);
		 Utilisateur u2 = new Utilisateur("farraji", "hicham", "hicham1", "0000",Role.Admin);
		 Utilisateur u3 = new Utilisateur("test", "tt", "test1", "0000",Role.user);

		 listUsers.add(u1);
		 listUsers.add(u2); 
		 listUsers.add(u3);
		 
	 }
	
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String login= request.getParameter("nom");
		String pass=request.getParameter("pass");
		Utilisateur user = null;
		boolean trouve=false;
		
		//========Test Si Utilisateur Saisi Est Existe Ou Non ====//
		
		for(Utilisateur u : listUsers) {
			
			if(u.getLogin().equalsIgnoreCase(login) && u.getPassword().equalsIgnoreCase(pass)) {
				HttpSession ses = request.getSession();
				ses.setAttribute("nom", u.getNom());
				ses.setAttribute("prenom",u.getPrenom());
				user=u;
				
				trouve=true;
				break;
			}
			
			
		}
	
		//========Test Si L'Utilisateur Est Un Admin Ou Un Utilisateur Simple ===//
		
		if(trouve) {
			
			if(user.getRole().equals(Role.Admin)) {
				response.sendRedirect("./Admin");

			}else {
				response.sendRedirect("./User");
			}
		}else {
			boolean n=true;
			request.setAttribute("found",n);
			doGet(request, response);
		}
		
	}

}
